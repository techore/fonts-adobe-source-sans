## Makefile downloads from git repository and extracts ttf/otf files. Use
## createdeb.sh to build debian package.

otfsrc=https://github.com/adobe-fonts/source-sans/releases/download/3.052R/OTF-source-sans-3.052R.zip
otfout=$(shell basename ${otfsrc})

all:
	test -f ${otfout} || wget ${otfsrc}
	unzip ${otfout}
	rm -rf __MACOSX/ ?tf/.DS_Store

clean:
	rm -f ${otfout}
	rm -rf OTF/
